//
//  expression.h
//  Spredsheet
//
//  Created by Michal Makarewicz on 3/27/14.
//  Copyright (c) 2014 Michal Makarewicz. All rights reserved.
//

#ifndef __Spreadsheet__expression__
#define __Spreadsheet__expression__

#include <string>
#include <functional>
#include <unordered_set>

namespace spreadsheet {
    // Forward declarations needed for CellReferenceExpression
    class Spreadsheet;
    class SpreadsheetCell;
    
    class Expression {
    public:
        virtual ~Expression() {};
        virtual double getValue() = 0;
        static std::unique_ptr<Expression> parseExpression (
            Spreadsheet* spreadsheet,
            std::string value, size_t first=0,
            size_t last=std::string::npos);
        // Computes list of all cells used in given expression
        virtual void getUsedCells(std::unordered_set<SpreadsheetCell*>& usedCells) = 0;
    };

    class ValueExpression : public Expression {
    public:
        virtual ~ValueExpression() {};
        ValueExpression(const double value);
        double getValue();
        void getUsedCells(std::unordered_set<SpreadsheetCell*>& usedCells);
    private:
        double value;
    };
    
    class BinaryExpression : public Expression {
    public:
        virtual ~BinaryExpression() {}
        BinaryExpression(std::function<double(double, double)> binaryOp,
                         std::unique_ptr<Expression>&& left,
                         std::unique_ptr<Expression>&& right);
        double getValue();
        void getUsedCells(std::unordered_set<SpreadsheetCell*>& usedCells);
    private:
        std::function<double(double, double)> binaryOp;
        std::unique_ptr<Expression> left;
        std::unique_ptr<Expression> right;
    };
    
    class CellReferenceExpression : public Expression {
    public:
        virtual ~CellReferenceExpression() {}
        CellReferenceExpression(std::weak_ptr<SpreadsheetCell> cell);
        double getValue();
        void getUsedCells(std::unordered_set<SpreadsheetCell*>& usedCells);
    private:
        std::weak_ptr<SpreadsheetCell> cell;
    };
}

#endif /* defined(__Spreadsheet__expression__) */
