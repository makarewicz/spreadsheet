//
//  spreedsheet.cpp
//  Spredsheet
//
//  Created by Michal Makarewicz on 3/27/14.
//  Copyright (c) 2014 Michal Makarewicz. All rights reserved.
//

#include "spreadsheet.h"
#include <iostream>

using namespace spreadsheet;

Spreadsheet::Spreadsheet():
spreadsheetCells(ROWS, std::vector<std::shared_ptr<SpreadsheetCell> >(COLUMNS)) {
    for (size_t i = 0; i < ROWS; ++i) {
        for (size_t j = 0; j < COLUMNS; ++j) {
            this->spreadsheetCells.at(i).at(j) = std::shared_ptr<SpreadsheetCell>(new SpreadsheetCell());
        }
    }
}

Spreadsheet::Spreadsheet(std::vector<std::vector<std::string>> spreadsheetCells) : Spreadsheet() {
    for (size_t i = 0; i < ROWS; ++i) {
        for (size_t j = 0; j < COLUMNS; ++j) {
            this->spreadsheetCells.at(i).at(j)->setValue(this, spreadsheetCells.at(i).at(j));
        }
    }
}

double Spreadsheet::getValue(size_t row, size_t column) {
    return spreadsheetCells.at(row).at(column)->getValue();
}

void Spreadsheet::setValue(size_t row, size_t column, std::string value) {
    spreadsheetCells.at(row).at(column)->setValue(this, value);
}

double Spreadsheet::getValue (std::string id) {
    std::pair<size_t, size_t> pos = idToPos(id);
    return getValue(pos.first, pos.second);
}

void Spreadsheet::setValue(std::string id, std::string value) {
    std::pair<size_t, size_t> pos = idToPos(id);
    setValue(pos.first, pos.second, value);
}

std::pair<size_t, size_t> Spreadsheet::idToPos(std::string id) {
    if (id.size() != 2 || id[0] < 'A' || id[0] > 'Z' || id[1] < '1' || id[1] > '9') {
        throw std::invalid_argument("Record id must be in form [A-Z][1-9] was \"" + id + "\"");
    }
    return std::pair<size_t, size_t>(id[0] - 'A', id[1] - '1');
}

std::weak_ptr<SpreadsheetCell> Spreadsheet::getSpreadsheetCell(size_t row, size_t column) {
    return spreadsheetCells.at(row).at(column);
}


std::weak_ptr<SpreadsheetCell> Spreadsheet::getSpreadsheetCell(std::string id) {
    std::pair<size_t, size_t> pos = idToPos(id);
    return getSpreadsheetCell(pos.first, pos.second);
}
