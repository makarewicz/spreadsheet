//
//  spreedsheet.h
//  Spredsheet
//
//  Created by Michal Makarewicz on 3/27/14.
//  Copyright (c) 2014 Michal Makarewicz. All rights reserved.
//

#ifndef __Spreadsheet__spreadsheet__
#define __Spreadsheet__spreadsheet__

#include <vector>
#include "spreadsheet_cell.h"

namespace spreadsheet {
    class Spreadsheet {
    public:
        // Rows are named A-Z
        const size_t ROWS = 26;
        // Columns are named 1-9
        const size_t COLUMNS = 9;
        // Creates spreadsheet ROWSxCOLUMNS and initializes it with 0s
        Spreadsheet ();
        // Creates spreadsheet ROWSxCOLUMS and assigns values from vector to cells
        Spreadsheet (std::vector<std::vector<std::string>> spreadsheetCells);
        // Computes value expression from given field
        double getValue (size_t row, size_t column);
        // Sets value for given field
        void setValue(size_t row, size_t column, std::string value);
        // Computes value expression from given field
        double getValue (std::string id);
        // Sets value for given field
        void setValue(std::string id, std::string value);
    private:
        // Translate id in form of [A-Z][1-9] to pair (row, column)
        std::pair<size_t, size_t> idToPos(std::string id);
        
        // Gets direct pointer to cell in spreadsheet.
        std::weak_ptr<SpreadsheetCell> getSpreadsheetCell(size_t row, size_t column);
        std::weak_ptr<SpreadsheetCell> getSpreadsheetCell(std::string id);
        
        std::vector<std::vector<std::shared_ptr<SpreadsheetCell> > > spreadsheetCells;
        
        // Friend function that needs direct access to spreadsheet cells for performance reasons
        // (minimizing updates caused by setValue)
        friend std::unique_ptr<Expression> Expression::parseExpression (
            Spreadsheet* spreadsheet, std::string value, size_t first, size_t last);
    };
    
}

#endif /* defined(__Spreadsheet__spreadsheet__) */
