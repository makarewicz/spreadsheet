//
//  expression.cpp
//  Spredsheet
//
//  Created by Michal Makarewicz on 3/27/14.
//  Copyright (c) 2014 Michal Makarewicz. All rights reserved.
//

#include "expression.h"
#include "spreadsheet.h"
#include <algorithm>

using namespace spreadsheet;

std::unique_ptr<Expression> Expression::parseExpression(
            Spreadsheet* spreadsheet, std::string value, size_t first, size_t last) {
    if (first == 0 && last >= value.size()) {
        // first call, remove whitespaces and normalize last.
        value.erase(remove_if(value.begin(), value.end(), isspace), value.end());
        last = std::min(last, value.length());
    }
    
    if (first == last) {
        // Special case, empty string
        return std::unique_ptr<Expression> (new ValueExpression(0));
    }
    
    std::vector<std::string> operatorsByPriority = {"+-", "*/"};
    
    // Using reverse iteratos since we're really looking for the last occurence
    std::string::reverse_iterator lastIt = std::string::reverse_iterator(value.begin() + first);
    std::string::reverse_iterator firstIt = std::string::reverse_iterator(value.begin() + last);

    std::string::reverse_iterator splitPosIt = lastIt;
    for (auto& operators: operatorsByPriority) {
        splitPosIt = std::find_first_of(firstIt, lastIt, operators.begin(), operators.end());
        if (splitPosIt != lastIt) {
            break;
        }
    }
    size_t splitPos = std::string::npos;
    if (splitPosIt != lastIt) {
        ++splitPosIt;
        splitPos = (splitPosIt.base() - value.begin());
    }
    if (splitPos < std::string::npos) {
        // Binary operator expression
        std::unique_ptr<Expression> left = parseExpression(spreadsheet, value, first, splitPos);
        std::unique_ptr<Expression> right = parseExpression(spreadsheet, value, splitPos + 1, last);
        std::function<double(double, double)> binaryOp;
        switch (value[splitPos]) {
            case '+':
                binaryOp = std::plus<double>();
                break;
            case '-':
                binaryOp = std::minus<double>();
                break;
            case '*':
                binaryOp = std::multiplies<double>();
                break;
            case '/':
                binaryOp = std::divides<double>();
                break;
        }
        return std::unique_ptr<Expression>(new BinaryExpression(binaryOp, std::move(left), std::move(right)));
    }
    if (isalpha(value[first])) {
        // Spreadsheet cell reference expression
        std::string id = value.substr(first, last - first);
        std::weak_ptr<SpreadsheetCell> cellReference = spreadsheet->getSpreadsheetCell(id);
        return std::unique_ptr<Expression>(new CellReferenceExpression(cellReference));
    }
    // Value expression
    size_t idx;
    double numericValue = std::stod(value.substr(first, last - first), &idx);
    if (idx != last - first) {
        // Something unexpected after number
        throw std::invalid_argument("expected double value, got " + 
                value.substr(first, last - first));
    }
    return std::unique_ptr<Expression> (new ValueExpression(numericValue));
}

ValueExpression::ValueExpression(const double value): value(value) {}
double ValueExpression::getValue() {
    return value;
}

void ValueExpression::getUsedCells(std::unordered_set<SpreadsheetCell*>& usedCells) {}

BinaryExpression::BinaryExpression(std::function<double(double, double)> binaryOp,
                                   std::unique_ptr<Expression>&& left,
                                   std::unique_ptr<Expression>&& right):
binaryOp(binaryOp), left(std::move(left)), right(std::move(right)) {}


double BinaryExpression::getValue() {
    return binaryOp(left->getValue(), right->getValue());
}

void BinaryExpression::getUsedCells(std::unordered_set<SpreadsheetCell*>& usedCells) {
    left->getUsedCells(usedCells);
    right->getUsedCells(usedCells);
}


CellReferenceExpression::CellReferenceExpression(std::weak_ptr<SpreadsheetCell> cell): cell(cell) {}

double CellReferenceExpression::getValue() {
    return cell.lock()->getValue();
}

void CellReferenceExpression::getUsedCells(std::unordered_set<SpreadsheetCell*>& usedCells) {
    usedCells.insert(cell.lock().get());
}
