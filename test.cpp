#include "spreadsheet.h"
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#define BOOST_TEST_MODULE SpreadsheetBasicTest
#include <boost/test/unit_test.hpp>

const double eps = 1e-6;

BOOST_AUTO_TEST_CASE (simple_case) {
    spreadsheet::Spreadsheet ss;
    BOOST_CHECK_CLOSE(ss.getValue("B3"), 0, eps);
    BOOST_CHECK_CLOSE(ss.getValue("A1"), 0, eps);
    ss.setValue("B3", "2.5");
    BOOST_CHECK_CLOSE(ss.getValue("B3"), 2.5, eps);
    BOOST_CHECK_CLOSE(ss.getValue("A1"), 0, eps);
}

BOOST_AUTO_TEST_CASE (whole_table_init_case) {
    std::vector<std::vector<std::string> > init(26, std::vector<std::string>(9, "2"));
    std::vector<std::vector<double> > expected(26, std::vector<double>(9, 2));
    init[0][0] = "1";
    expected[0][0] = 1;
    init[0][1] = "A1 + 3 * 2";
    expected[0][1] = 7;
    init[2][3] = " A1*A2 + A3 *2";
    expected[2][3] = 11;
    spreadsheet::Spreadsheet ss(init);
    for (int i = 0; i < 26; ++i) {
        for (int j = 0; j < 9 ; ++j) {
            BOOST_CHECK_CLOSE(ss.getValue(i, j), expected[i][j], eps);
        }
    }
}
std::pair<size_t, size_t> nth_pos(size_t n) {
    return std::make_pair(n / 9, n % 9);
}

std::string nth_id(size_t n) {
    auto p = nth_pos(n);
    std::string res("--");
    res[0] = char(p.first+ 'A');
    res[1] = char(p.second + '1');
    return res;
}
BOOST_AUTO_TEST_CASE (fibonnaci) {
    double fibonacci[39] = {
        0, 1, 1, 2, 3, 5, 8, 13, 21, 34,
        55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181,
        6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229,
        832040, 1346269, 2178309, 3524578, 5702887, 9227465, 14930352, 24157817, 39088169}; // 0-38 fib
    spreadsheet::Spreadsheet ss;
    ss.setValue(0, 0, "0");
    ss.setValue(0, 1, "1");
    for (size_t i = 2; i <= 38; ++i) {
        std::pair<size_t, size_t> pos = nth_pos(i);
        std::string prev1 = nth_id(i-1);
        std::string prev2 = nth_id(i-2);
        ss.setValue(pos.first, pos.second, prev1 + "+" + prev2);
    }
    for (size_t i = 0; i <= 38; ++i) {
        std::pair<size_t, size_t> pos = nth_pos(i);
        BOOST_CHECK_CLOSE(ss.getValue(pos.first, pos.second), fibonacci[i], eps);
    }
    BOOST_CHECK_CLOSE(ss.getValue(1, 5), fibonacci[14], eps);
    ss.setValue(0, 1, "0");
    BOOST_CHECK_CLOSE(ss.getValue(1, 5), 0, eps);
    ss.setValue(0, 3, "1");
    BOOST_CHECK_CLOSE(ss.getValue(1, 5), fibonacci[12], eps);
}

BOOST_AUTO_TEST_CASE (arithmetics) {
    spreadsheet::Spreadsheet ss;
    BOOST_CHECK_CLOSE(ss.getValue("A1"), 0, eps);
    ss.setValue("A1", "1 - 2 - 5");
    BOOST_CHECK_CLOSE(ss.getValue("A1"), -6, eps);
    ss.setValue("A1", "-1 + 3 * 5");
    BOOST_CHECK_CLOSE(ss.getValue("A1"), 14, eps);
    ss.setValue("A1", "10 * 2.5 + 11");
    BOOST_CHECK_CLOSE(ss.getValue("A1"), 36, eps);
    ss.setValue("A1", "10 / 10 / 5 * 2" );
    BOOST_CHECK_CLOSE(ss.getValue("A1"), 0.4, eps);
}

BOOST_AUTO_TEST_CASE (circular_reference) {
    spreadsheet::Spreadsheet ss;
    ss.setValue("A1", "A2");
    BOOST_CHECK_CLOSE(ss.getValue(0, 0),  0, eps);
    ss.setValue("A2", "A1");
    BOOST_CHECK_THROW(ss.getValue(0, 0), spreadsheet::CircularReferenceException)
}

BOOST_AUTO_TEST_CASE (wrong_input) {
    spreadsheet::Spreadsheet ss;
    BOOST_CHECK_THROW(ss.setValue("A1", "4d"), std::invalid_argument);
    BOOST_CHECK_THROW(ss.setValue("A1", "5 % 2"), std::invalid_argument);
    BOOST_CHECK_THROW(ss.setValue("A1", "(5 + 2) * 2"), std::invalid_argument);
    BOOST_CHECK_THROW(ss.setValue("A1", "LOOOOL"), std::invalid_argument);
    BOOST_CHECK_CLOSE(ss.getValue("A1"), 0, eps);
}
