//
//  main.cpp
//  Spredsheet
//
//  Created by Michal Makarewicz on 3/27/14.
//  Copyright (c) 2014 Michal Makarewicz. All rights reserved.
//

#include <iostream>
#include "spreadsheet.h"
#include "expression.h"

int main(int argc, const char * argv[])
{
    // insert code here...
    std::cout << "Simple spreadsheet simulator, syntax:\n"
              << "FieldName=Formula  <- assign formula to field\n"
              << "Formula  <- compute formula and output result\n";
    std::string line;
    spreadsheet::Spreadsheet ss;
    while (std::getline(std::cin, line)) {
        line.erase(remove_if(line.begin(), line.end(), [](const char& c){return isspace(c) || iscntrl(c);}), line.end());
        size_t pos = line.find("=");
        if (pos != std::string::npos) {
            ss.setValue(line.substr(0, pos), line.substr(pos + 1));
        } else {
            std::cout << spreadsheet::Expression::parseExpression(&ss, line)->getValue() << std::endl;
        }
    }
    return 0;
}

