CXX=g++
CPPFLAGS=-std=c++11 -g
LDFLAGS=
LDLIBS=-lboost_unit_test_framework
LSRCS=expression.cpp spreadsheet.cpp spreadsheet_cell.cpp 
LOBJS=$(subst .cpp,.o,$(LSRCS))
SRCS=expression.cpp spreadsheet.cpp spreadsheet_cell.cpp command_line_tool.cpp test.cpp
OBJS=$(subst .cpp,.o,$(SRCS))

all: command_line_tool

command_line_tool: $(LOBJS) command_line_tool.o
	$(CXX) $(LDFLAGS) -o command_line_tool $(LOBJS) command_line_tool.o $(LDLIBS)

test: $(LOBJS) test.o
	$(CXX) $(LDFLAGS) -o test $(LOBJS) test.o $(LDLIBS)
	./test

depend: .depend

.depend: $(SRCS)
	rm -f ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

dist-clean: clean
	$(RM) *~ .dependtool

include .depend
