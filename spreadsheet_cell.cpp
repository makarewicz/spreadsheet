//
//  spreadsheet_cell.cpp
//  Spredsheet
//
//  Created by Michal Makarewicz on 3/27/14.
//  Copyright (c) 2014 Michal Makarewicz. All rights reserved.
//

#include "spreadsheet_cell.h"

using namespace spreadsheet;

SpreadsheetCell::SpreadsheetCell(): expression(new ValueExpression(0)), cacheStatus(OUTDATED) {}

double SpreadsheetCell::getValue() {
    if (cacheStatus == CORRECT) {
        return cachedValue;
    }
    if (cacheStatus == COMPUTING) {
        throw CircularReferenceException();
    }
    cacheStatus = COMPUTING;
    cachedValue = expression->getValue();
    cacheStatus = CORRECT;
    return cachedValue;
}

void SpreadsheetCell::setValue(Spreadsheet* spreadsheet, const std::string value) {
    std::unordered_set<SpreadsheetCell*> usedCells;
    expression->getUsedCells(usedCells);
    for (SpreadsheetCell* usedCell: usedCells) {
        usedCell->dependants.erase(this);
    }
    expression = Expression::parseExpression(spreadsheet, value);
    usedCells.clear();
    expression->getUsedCells(usedCells);
    for (SpreadsheetCell* usedCell: usedCells) {
        usedCell->dependants.insert(this);
    }
    retireCache();
}

void SpreadsheetCell::retireCache() {
    if (cacheStatus == OUTDATED) {
        return;
    }
    cacheStatus = OUTDATED;
    for (SpreadsheetCell* dependant: dependants) {
        dependant->retireCache();
    }
}