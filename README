Overview:
Spreadsheet is represented by class Spreadsheet as 2 dimentional array of
SpreadsheetCells. Each cell holds it's own expression tree as parsed on
setValue method. Concrete value of the spreadsheet's cell is computed
lazily on getValue call, and stored into cache (inside SpreadsheetCell field),
to be used in consecutive calls of getValue. Each cell takes care of informing
different cells if it uses them inside it's expression. If value inside a cell
is changed we mark cache as outdated (to be recomputed on next getValue call)
and inform all cells that use changed cell in their computation that their
value is changed as well.

Error handling:
If we cannot parse input (e.g. wrong characters, wrong indexes), we throw
std::invalid_argument and incorrect value is not assigned to cell.
If there is a circular reference inside cells (e.g. A1 = A2, A2 = A1),
we throw CircularReferenceException during getValue call

Possible improvements:
We could externalize grammar of expressions and use general purpose parser
for expressions (e.g. http://bnfc.digitalgrammars.com/). I didn't do it because
I though it would be a bit of an overkill for the given task.
