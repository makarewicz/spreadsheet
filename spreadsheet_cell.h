//
//  spreadsheet_cell.h
//  Spredsheet
//
//  Created by Michal Makarewicz on 3/27/14.
//  Copyright (c) 2014 Michal Makarewicz. All rights reserved.
//

#ifndef __Spreadsheet__spreadsheet_cell__
#define __Spreadsheet__spreadsheet_cell__

#include <vector>
#include "expression.h"
#include <unordered_set>

namespace spreadsheet {
    class Spreadsheet;
    class CircularReferenceException : public std::exception {
        virtual const char* what() const throw() {
            return "Circular reference in graph detected.";
        }
    };
    class SpreadsheetCell {
    public:
        // Default constructor, initializes value to 0
        SpreadsheetCell();
        // Computes value from cell, uses cache if possible
        double getValue();
        // Parses and sets value to cell
        void setValue(Spreadsheet* spreadsheet, const std::string value);
    private:
        // We need 3 statuses to prevent circular references in cells
        enum CacheStatus {
            CORRECT,
            COMPUTING,
            OUTDATED
        };
        // Cache for reducing computation
        CacheStatus cacheStatus;
        double cachedValue;
        // Parsed expresssion held by cell
        std::unique_ptr<Expression> expression;
        // Set of all cells which result depents on this cell, used to retire cache
        std::unordered_set<SpreadsheetCell*> dependants;
        // Sets cache status to outdated for this cell and all cells that use this cell
        // in their formula (be it directly or indirectly)
        void retireCache();
    };
}
#endif /* defined(__Spreadsheet__spreadsheet_cell__) */
